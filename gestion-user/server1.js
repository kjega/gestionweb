let http = require ('http')
let fs = require ('fs')
let express= require ('express')
let bodyParser = require('body-parser')
let app = express()

// create application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false })

// EJS engine
app.set('view engine', 'ejs')

//tout les css,js, img dans ce doisser
app.use('/', express.static('public'))


//router
 app.get('/', function(request, response){
     response.render('pages/index', {H1title:'Les capétiens', parallaxImg: '/image/roi.png'})
   })    
 app.get('/philippeAuguste/', function(request, response){
    response.render('pages/philippeAuguste', {H1title:'Philippe August', parallaxImg: 'image/para.jpg'})
 })
 
app.get('/louis/', function(request, response){
   response.render('pages/louis', {H1title:'Louis ix', week6Cap: 'salutJJJJJJ louis', parallaxImg: 'image/louis3.jpg'})
})
app.get('/huguesCap/', function(request, response){
  response.render('pages/huguesCap', {H1title:'Hugues Capet', parallaxImg: 'image/hugues-capet-01.jpg'})
})
app.get('/philippeBel/', function(request, response){
  response.render('pages/philippeBel', {H1title:'Philippe le Bel', parallaxImg: 'image/pa.jpg'})
})

 
 app.get('/contact/', function(request, response){
   response.render('pages/contact', { H1title:'Members Form'})
 })

//to collect all data of the clients
 app.post('/contact', urlencodedParser, function(request, response){
    console.log(request.body);
   response.render('pages/contact-success', {data: request.body})
 
//creation of json
 let client = (request.body);
  let json = JSON.stringify(client);
   
 // this code allow to creat a file json automatic then we can write into
 //last details of clients. we cant save the old details  
  fs.writeFile('members.json', json, function(err){
       if (err) throw err;
       console.log('we use this function for creat a json file automatic');
    });
//to add the details of clients into the json file
  fs.appendFile('members.json', json, function(err){
   if (err) throw err;
   console.log('File created!');
  });
  
 })


app.use(function(request, response, next){
   response.status(404);
    response.send('404: 404 Page not found')
  });
  
  

//Porte
app.listen(8084)


// let http = require ('http')
// let fs = require ('fs')
// let server = http.createServer()
// server.on('request', function(request, response) {
//     console.log('salut')
//     fs.readFile('index1.html', function(err,data){
//         response.writeHead(200, {
//             'Contente-type':'text/html; charset-utf-8'
//         })
//         response.end(data)
//     
// })
// server.listen(8081)